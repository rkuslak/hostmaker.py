#!/usr/bin/env python3
"""
    hostsupdate.py
    A program to combine and manage hosts.txt files
    Copyright (C) 2018 Ron Kuslak

    hostsupdate.py is free software: you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the Free
    Software Foundation version 2 of the License.

    hostsupdate.py is distributed in the hope that it will be useful, but
    WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
    or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License along
    with Foobar. If not, see <http://www.gnu.org/licenses/>.

    A simple script to download and combine multiple hosts.txt files into one.
    Allows for specifying hostnames to always exclude from being listed, and
    ones that will be listed even without having a entry in any file. It is
    intended as a sort of 'bad actors' blocking measure at the DNS level for
    preventing access to various ad and malware sites.
"""

import socket
import logging
import urllib
import urllib.request
import ipaddress
import itertools
from typing import List, Dict, Any, Union

logger = logging.getLogger(__name__)

HostsDict = Dict[str, List[str]]
AddressType = Union[ipaddress.IPv4Address, ipaddress.IPv6Address]


class HostsContainer(object):
    def __init__(self, ignored_hosts_list: List[str] = []):
        self._ignored_hosts_list = ignored_hosts_list
        self._hosts: HostsDict = dict()

    def add_host(self, host: str):
        cleaned_host = str(host).strip()

        if host in self._ignored_hosts_list:
            logger.warning("Attempted to add ignored host %s", host)
            return

        host_key = cleaned_host
        if "." in host_key:
            host_key = ".".join(host_key.split(".")[-2:])

        if not self._hosts.get(host_key) or host in self._hosts[host_key]:
            self._hosts[host_key] = self._hosts.get(host_key, []) + [cleaned_host]

    def create_hosts_file(self, replacementIP: str) -> str:
        """
        Returns a hosts.txt file with 1 line per domain in the current
        collection of hosts
        """

        results: List[str] = list()

        for key in sorted(self._hosts.keys()):
            sorted_host_domains = [domain for domain in sorted(self._hosts[key])]
            domains = " ".join(sorted_host_domains)
            results.append(f"{replacementIP} {domains}")

        return "\n".join(results)

    def __add__(self, value: Any):
        if isinstance(value, str):
            self.add_host(value)
            return

        if isinstance(value, list):
            for host in value:
                self.add_host(host)
            return

        if isinstance(value, HostsContainer):
            addition = value._hosts

            for key in [k for k in addition.keys() if k not in self._hosts.keys()]:
                self._hosts[key] = []

            for key in addition.keys():
                for domain in addition[key]:
                    if domain not in self._hosts[key]:
                        self._hosts[key] += [domain]
            return

        raise ValueError(f"No means to add passed value {value} ({type(value)})")


class HostFileDataLoader(object):
    def __init__(self):
        self._hosts = list()

    def _is_valid_ip_address(
        self, test_str: str, require_localhost: bool = True
    ) -> bool:
        """
        Validates that the passed string contains a valid IP address pointing to
        localhost or a null address for a host file.
        """

        try:
            addr: AddressType = ipaddress.IPv4Address(test_str)
        except ipaddress.AddressValueError:
            try:
                addr = ipaddress.IPv6Address(test_str)
            except ipaddress.AddressValueError:
                return False

        # Special case for 0.0.0.0, as it is often used to add a "sink" for addresses
        # in hosts files
        if require_localhost and not (addr.is_loopback or addr.exploded == "0.0.0.0"):
            return False

        return True

    def get_hosts(self) -> List[str]:  # pragma: no cover
        raise NotImplementedError

    def parse_host_file_data(self, data: Union[str, List[str]]) -> List[str]:
        # strip comments
        if isinstance(data, str):
            lines = [line.strip() for line in data.split("\n")]
        else:
            lines = data
        lines = [line.split("#")[0] for line in data if line]

        # strip empty lines or lines without a valid hostfile entry
        host_lines = [line.split() for line in lines if line]
        host_lists = [
            line[1:]
            for line in host_lines
            if len(line) > 1 and self._is_valid_ip_address(line[0], True)
        ]

        lines = list(itertools.chain.from_iterable(host_lists))

        return lines


class URLLoader(HostFileDataLoader):
    def __init__(self, url: str, *args, **kwargs):
        HostFileDataLoader.__init__(self)
        self._url = url

    def get_hosts(self) -> List[str]:
        try:
            logging.debug(f'Requesting URL: "{self._url}"')
            with urllib.request.urlopen(self._url) as request:
                data_bytes = request.read()

            payload = data_bytes.decode(encoding="utf-8", errors="replace").split("\n")
            data = [line.strip() for line in payload]

        except (IOError, OSError) as ex:
            # Connection to URL failed; move on and return nothing
            logger.error("Unable to download file " + self._url)
            logger.error(ex)
            return []

        except UnicodeDecodeError:
            # Last ditch if something go TERRIBLY wrong with decoding; log and
            # return nothing
            logging.error(f'Failed to parse response from "{self._url}"')
            return []

        return self.parse_host_file_data(data)


if __name__ == "__main__":  # pragma: no cover
    # A list of URLs which have host files we are interested in combining. Note: please
    # do not use this as part of some automated thing.
    HOSTS_FILES_LIST = [
        "http://winhelp2002.mvps.org/hosts.txt",
        "https://hosts-file.net/download/hosts.txt",
    ]

    hosts_txt_destination = "./hosts"

    # Replacement IP to use
    REPLACEMENT_IP = "0.0.0.0"

    # List of domains never add, even if included in a hosts file
    DOMAINS_IGNORED: List[str] = [
        "localhost",
        "localhost.localdomain",
        socket.gethostname(),
    ]

    # List of domains to always add, even if not included in a hosts file
    # TODO: This is currently a noop
    DOMAINS_REQUIRED: List[str] = []

    hosts_container = HostsContainer(ignored_hosts_list=DOMAINS_IGNORED)

    logging.basicConfig(format="%(asctime)s - %(name)s [%(levelname)s] %(message)s")

    for url in HOSTS_FILES_LIST:
        url_loader = URLLoader(url)
        hosts = [host for host in url_loader.get_hosts()]
        hosts_container + hosts

    with open(hosts_txt_destination, "w") as hosts_txt:
        print(hosts_container.create_hosts_file("0.0.0.0"), file=hosts_txt)
