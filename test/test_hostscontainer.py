import pytest
import hostsupdate
import unittest


@pytest.mark.parametrize(
    "test_name, starting_set, additions, want",
    [
        ("empty_start_with_ignored_item", ["localhost"], ["foo"], {"foo": ["foo"]}),
        ("same_join", ["foo"], ["foo"], {"foo": ["foo"]}),
        (
            "set_with_duplicates_and_long_element",
            ["foo", "bar.foo", "longer.bar.foo"],
            ["foo"],
            {"foo": ["foo"], "bar.foo": ["bar.foo", "longer.bar.foo"]},
        ),
    ],
)
def test_hostmaker_addhost(test_name, starting_set, additions, want):
    host_container = hostsupdate.HostsContainer(ignored_hosts_list=["localhost"])
    addition_container = hostsupdate.HostsContainer()

    if starting_set:
        for item in starting_set:
            host_container + item

    if additions:
        for item in additions:
            addition_container + item

    host_container + addition_container

    unittest.TestCase().assertCountEqual(host_container._hosts, want)


def test_hostmaker_add_list():
    host_container = hostsupdate.HostsContainer(ignored_hosts_list=["localhost"])
    host_container + ["foo", "foo.bar", "localhost"]

    unittest.TestCase().assertCountEqual(
        host_container._hosts, {"foo": ["foo"], "foo.bar": ["foo.bar"]}
    )


def test_assert_on_invalid_addition():
    host_container = hostsupdate.HostsContainer()

    with unittest.TestCase().assertRaises(ValueError):
        host_container + 1
