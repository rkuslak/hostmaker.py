import pytest
import hostsupdate


@pytest.mark.parametrize(
    "test_name, have, need_local, expect",
    [
        ("localhost", "127.0.0.1", True, True),
        ("invalid_str", "a.test", True, False),
        ("invalid_str", "a.test", False, False),
        ("localhost_ipv6", "::1", True, True),
        ("localhost", "0.0.0.0", True, True),
        ("remote_ip", "123.123.123.123", True, False),
        ("remote_ip_ip_only", "123.123.123.123", False, True),
    ],
)
def test_is_valid_ip(test_name, have, need_local, expect):
    url_parser = hostsupdate.URLLoader("")

    result = url_parser._is_valid_ip_address(have, need_local)
    assert result == expect, f"{test_name}:\nWANTED: {expect}\nGOT: {result}"
